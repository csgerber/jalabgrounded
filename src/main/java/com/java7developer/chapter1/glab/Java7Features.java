/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.java7developer.chapter1.glab;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author ag
 */
public class Java7Features {

    public static void main(String[] args) {

        //Strings in switch() example
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter day of week:");
        printDay(scanner.nextLine());
        
         //binary and hex with underscores example
        System.out.println(getInteger());
        
        ArrayList<String> cities = getCities();
        for (String city : cities) {
            System.out.println(city);
        }
        
        
    }
    
    
    
    

    private static void printDay(String dayOfWeek) {
        switch (dayOfWeek.toLowerCase()) {
            case "sunday":
                System.out.println("Dimanche");
                break;
            case "monday":
                System.out.println("Lundi");
                break;
            case "tuesday":
                System.out.println("Mardi");
                break;
            case "wednesday":
                System.out.println("Mercredi");
                break;
            case "thursday":
                System.out.println("Jeudi");
                break;
            case "friday":
                System.out.println("Vendredi");
                break;
            case "saturday":
                System.out.println("Samedi");
                break;
            default:
                System.out.println("Error: '" + dayOfWeek + "' is not a day of the week");
                break;
        }
    }
    
    
    private static String getInteger(){
           
            //241 in binary. Notice that I can use optional underscores to help me read it. The leading zeros are optional, 
            //though I can only have 32  of them total as int is a 32bit number in Java
    
            int nBinary = 0b0000_0000_0000_0000_0000_0000_1111_0001;
            int nHex = 0x0_0_0_0_0_0_F_1;
            
            return String.valueOf(nBinary - nHex);
       }
    
    
    //use of diamond syntax
    private static ArrayList<String> getCities(){
        
        //noticet that I don't need to declare the "String" in the right side of this expression
        ArrayList<String> arrayList = new ArrayList<>();
        
        arrayList.add("Chicago");
        arrayList.add("New York");
        arrayList.add("Boston");
        arrayList.add("Toronto");
        
        return arrayList;
        
        
    }

}
