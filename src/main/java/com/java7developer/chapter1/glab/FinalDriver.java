/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.java7developer.chapter1.glab;

/*
 Java is a managed environment, which means that objects are garbage-collected for you. 
 You don't have to worry about de-allocated objects off the heap

 */


//http://howtodoinjava.com/2012/10/31/why-not-to-use-finalize-method-in-java/
import java.util.logging.Level;
import java.util.logging.Logger;


/*
 This example demonstrates the use of the garbage collector. We don't have direct control over the 
 de-allocation of memory in Java. The closest mechanism we have is the finalize method which is a callback that occurs 
 directly before an object is garbage collected. 

 In our example, each MyThread object is instantiated with an integer so that we can report on its status


 */
public class FinalDriver {

    public static void main(String args[]) {
        for (int i = 0; i < 20; i++) {
            new Thread(new MyThread(i)).start();
        }

        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            Logger.getLogger(FinalDriver.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.gc();
    }

    private static class MyThread implements Runnable {

        private int mNum;

        public int getNum() {
            return mNum;
        }

        public void setNum(int mNum) {
            this.mNum = mNum;
        }

        public MyThread(int mNum) {
            this.mNum = mNum;
        }

        @Override
        protected void finalize() throws Throwable {
            System.out.println("In finalize block of " + getNum());
            super.finalize();
        }

        @Override
        public void run() {
            try {
                testMethod();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void testMethod() throws InterruptedException {

            try {
                System.out.println("In try block of " + getNum());

            } catch (Throwable throwable) {
                System.out.println("In catch block");

            }

        }
    }
}
