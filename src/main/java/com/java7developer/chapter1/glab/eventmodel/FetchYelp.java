/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.java7developer.chapter1.glab.eventmodel;

import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author ag
 */
public class FetchYelp extends javax.swing.JFrame {

    /**
     * Creates new form FetchYelp
     */
    public FetchYelp() {
        initComponents();
        mSearchButton.setEnabled(false);
        mListModel = new DefaultListModel();
        mList.setModel(mListModel);

        setTitle("Advanced Swing and Java Event Model");
        mList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {

                if (e.getValueIsAdjusting()) {
                     System.out.println(((JList) e.getSource()).getSelectedValue());
                }
                
            }
        });

        mCityText.addKeyListener(new TextWatcher(mSearchText));
        mSearchText.addKeyListener(new TextWatcher(mCityText));

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        mSearchText = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        mCityText = new javax.swing.JTextField();
        mSearchButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        mList = new javax.swing.JList();
        mProgressBar = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Search Term for Place:");

        mSearchText.setToolTipText("e.g. Arboretum or Mexican Food");

        jLabel2.setText("City or zip:");

        mCityText.setToolTipText("e.g. Chicago or 60616");

        mSearchButton.setText("Search");
        mSearchButton.setToolTipText("");
        mSearchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mSearchButtonActionPerformed(evt);
            }
        });

        jLabel3.setText("Results:");

        mList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(mList);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(mProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mSearchButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mSearchText)
                            .addComponent(mCityText)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel3))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(mSearchText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(mCityText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(mSearchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(mProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mSearchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mSearchButtonActionPerformed
        // TODO add your handling code here:

        //use the Yelp API to fetch 
        mListModel.removeAllElements();
        mProgressBar.setValue(0);
        YelpSwingWorker worker = new YelpSwingWorker(mSearchText.getText(), mCityText.getText());
        worker.execute();

    }//GEN-LAST:event_mSearchButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FetchYelp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FetchYelp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FetchYelp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FetchYelp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FetchYelp().setVisible(true);
            }
        });
    }

    private class YelpSwingWorker extends SwingWorker<Void, Integer> {

        private String mPlace, mCity;
        private YelpSearchResults yelpSearchResultLocal = null;

        public YelpSwingWorker(String mPlace, String mCity) {
            this.mPlace = mPlace;
            this.mCity = mCity;

        }

        @Override
        protected Void doInBackground() throws Exception {

            Yelp yelpApi = new Yelp();
            yelpSearchResultLocal = yelpApi.searchMultiple(mPlace, mCity);

            return null;

        }

        @Override
        protected void done() {

            for (String str : yelpSearchResultLocal.getSimpleValues()) {
                mListModel.addElement(str);
            }
            mProgressBar.setValue(100);

        }
    }

    private class TextWatcher extends KeyAdapter {

        private JTextField otherField;

        public TextWatcher(JTextField otherFieldParam) {
            this.otherField = otherFieldParam;

        }

        private boolean hasText(JTextField textField) {
            if (textField.getText().toString().equalsIgnoreCase("")) {
                return false;
            } else {
                return true;
            }
        }

        public void keyReleased(KeyEvent e) {
            if (hasText((JTextField) e.getSource()) && hasText(otherField)) {
                mSearchButton.setEnabled(true);
            } else {
                mSearchButton.setEnabled(false);
            }
        }

    }

    private DefaultListModel mListModel;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField mCityText;
    private javax.swing.JList mList;
    private javax.swing.JProgressBar mProgressBar;
    private javax.swing.JButton mSearchButton;
    private javax.swing.JTextField mSearchText;
    // End of variables declaration//GEN-END:variables
}
