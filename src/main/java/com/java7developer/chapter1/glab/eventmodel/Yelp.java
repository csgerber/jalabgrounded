package com.java7developer.chapter1.glab.eventmodel;


import com.google.gson.Gson;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;



/**
 * Created by Adam Gerber on 5/20/2014.
 * adam.gerber@yahoo.com
 */
//see instructions for the classes you need here: http://stackoverflow.com/questions/6676733/yelp-integration-in-android (see step 3)

/**
 * Example for accessing the Yelp API.
 */
public class Yelp {

    //these are my developers keys, please get your own by registering here: http://www.yelp.com/developers/getting_started/api_access
    private static final String CONSUMER_KEY = "dSZgGbpE51gcJ2mPFy8Dag";
    private static final String CONSUMER_SECRET = "CAe7Yp1NEYVPh2Z2ZpDDetqUpWM";
    private static final String TOKEN = "ksJ-aFEUA-sO8YKI9TwbTem8DoLOOtH0";
    private static final String TOKEN_SECRET = "O1oqDGf93zFEz-_ctYgicO1VYQM";

    OAuthService service;
    Token accessToken;



    public Yelp(){
        this.service = new ServiceBuilder().provider(YelpApi2.class).apiKey(CONSUMER_KEY).apiSecret(CONSUMER_SECRET).build();
        this.accessToken = new Token(TOKEN, TOKEN_SECRET);
    }

    public YelpSearchResults searchMultiple(String searchTerm, String city) {

        // Execute a signed call to the Yelp service.
        OAuthService service = new ServiceBuilder().provider(YelpApi2.class).apiKey(CONSUMER_KEY).apiSecret(CONSUMER_SECRET).build();
        Token accessToken = new Token(TOKEN, TOKEN_SECRET);
        OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.yelp.com/v2/search");

        request.addQuerystringParameter("location", city);
        //request.addQuerystringParameter("category", "restaurants");
        request.addQuerystringParameter("term", searchTerm);
        request.addQuerystringParameter("limit", "20");

        service.signRequest(accessToken, request);
        Response response = request.send();
        String rawData = response.getBody();

        YelpSearchResults mYelpSearchResult = null;

        try {
            mYelpSearchResult = new Gson().fromJson(rawData, YelpSearchResults.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mYelpSearchResult;
    }
}
