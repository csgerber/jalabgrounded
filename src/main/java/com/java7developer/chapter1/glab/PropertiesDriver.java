/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.java7developer.chapter1.glab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ag
 */
//http://stackoverflow.com/questions/5599210/saving-properties-in-a-file-with-java-formatted
public class PropertiesDriver {

    private static Properties props = null;
    private static FileInputStream input = null;
    private static FileOutputStream output = null;
    
    
    public static void main(String[] args) {

        String strUserDir = System.getProperties().getProperty("user.dir");
        String strConifFilePath = strUserDir + "/config.properties";

        //you could make this a singleton and syncrhonize calls to get and set properties
        props = new Properties();
        try {
            
            File file = new File(strConifFilePath); 
            if (!file.exists()){
                 file.createNewFile(); 
            }
                
            
            output = new FileOutputStream(file);

            props.put("user", "agerbger");
            props.put("password", "java");
            props.put("port", "8080");

            props.store(output, null);
        
        
          
        input = new FileInputStream(file);
        
        props.load(input);
            System.out.println(file.getAbsolutePath());
            for (String key : props.stringPropertyNames()) {
                String value = props.getProperty(key);
                System.out.println(key + " => " + value);
            }
        } catch (Exception e) {

            System.out.println(e.getMessage());
        } finally {
            try {
                input.close();
                output.close();
            } catch (IOException ex) {
                Logger.getLogger(PropertiesDriver.class.getName()).log(Level.SEVERE, null, ex);
            }
          
        }

    }

    
}
