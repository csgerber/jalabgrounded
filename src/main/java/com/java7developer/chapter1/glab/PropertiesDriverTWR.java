/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.java7developer.chapter1.glab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ag
 */

public class PropertiesDriverTWR {

    private static Properties props = null;
//    private static FileInputStream input = null;
//    private static FileOutputStream output = null;
    
    
    public static void main(String[] args) {


        //you could make this a singleton and syncrhonize calls to get and set properties
        props = new Properties();
        String strUserDir = System.getProperties().getProperty("user.dir");
        String strConifFilePath = strUserDir + "/config.properties";
        File file = new File(strConifFilePath); 
        if (!file.exists()){
            try { 
                //will only create one if it doesn't already exist
                file.createNewFile();
            } catch (IOException ex) {
                return;
            }
         }
        
        
        //we can't put file.createNewFile() between parens becuase it must implment closable
        try 
            (
              //declare and instantiate any resources here
              FileOutputStream output = new FileOutputStream(file);
              FileInputStream input = new FileInputStream(file);
            )
        {
            
        
            props.put("path", "/user/something/something");
            props.put("server", "sparc2100");
            props.put("domain", "uchicago.edu");

            props.store(output, null);
     
        
          
            props.load(input);
            for (String key : props.stringPropertyNames()) {
                String value = props.getProperty(key);
                System.out.println(key + " => " + value);
            }
        } catch (Exception e) {

            System.out.println(e.getMessage());
        } 

    }

    
}
