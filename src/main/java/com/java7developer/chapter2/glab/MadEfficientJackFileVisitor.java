package com.java7developer.chapter2.glab;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.nio.file.StandardOpenOption;

public class MadEfficientJackFileVisitor extends SimpleFileVisitor<Path> {


	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		if (file.toString().endsWith(".backup")) {
                    
                    //to surround with try/catch, select Source || Fix Code
                    try {
                        Files.delete(file);
                    } catch (IOException iOException) {
                        System.err.println(iOException.getMessage());
                    }
		}
		return FileVisitResult.CONTINUE;
	}

}
