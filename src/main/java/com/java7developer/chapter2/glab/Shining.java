package com.java7developer.chapter2.glab;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

//lab developed by Drew Boshardy and Adam Gerber
public class Shining {
    
    static DecimalFormat decimalFormatNano = new DecimalFormat("#,### nanoseconds");
    static DecimalFormat decimalFormatSec = new DecimalFormat("#.######################### seconds");

	public static void main(String[] args) {

                 //this is the maven/git/netbeans project root dir
                String strUserDir = System.getProperties().getProperty("user.dir");
            
		Path filesDir = Paths.get(strUserDir + "/mydir"); // This works regardless of OS.
		System.out.println(filesDir.toString());

		try {
			//synchronous process
			long startTime = System.nanoTime();
			Files.walkFileTree(filesDir, new JackFileVisitor());
			long endTime = System.nanoTime();
			long duration = endTime - startTime;
			System.out.println("Synchronous process time-to-completion: " + decimalFormatNano.format(duration));

			//async process
			startTime = System.nanoTime();
			Files.walkFileTree(filesDir, new EfficientJackFileVisitor());
			endTime = System.nanoTime();
			long asyncDuration = endTime - startTime;
			System.out.println("Asynchronous process time-to-completion: " + decimalFormatNano.format(asyncDuration));

			//time difference to complete
			long diff = duration - asyncDuration;
			System.out.println("Time difference = " + decimalFormatNano.format(diff) + " or " + decimalFormatSec.format(diff/1000000000.0));
                        
                        
                        //delete all the backup files
                        if (false){
                            Files.walkFileTree(filesDir, new MadEfficientJackFileVisitor());
                        }
                        
                        
                        
		} catch (IOException ex) {
			Logger.getLogger(Shining.class.getName()).log(Level.SEVERE, null, ex);
		}
	}


}
