package com.java7developer.chapter2.glab;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.FileChannel;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.nio.file.StandardOpenOption;

public class JackFileVisitor extends SimpleFileVisitor<Path> {

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		if (file.toString().endsWith(".txt")) {
			//overwrite the file to appease the voices in your head
			//not featured: hacking down door with fire axe. See next
			//release!
			ByteBuffer buffer = ByteBuffer.allocate(100_000);
			FileChannel channel = FileChannel.open(file, StandardOpenOption.READ);
			int result = channel.read(buffer, 0);
			Path dest = Paths.get(file.toString() + ".backup");
			if (!Files.exists(dest)) {
				Files.createFile(dest);
			}

			FileChannel channelWrite = FileChannel.open(dest, StandardOpenOption.WRITE, StandardOpenOption.CREATE );
			buffer.flip();
			
			channelWrite.write(buffer, 0);
			System.out.println("Bytes read [" + result + "]");

		}
		return FileVisitResult.CONTINUE;
	}

}
