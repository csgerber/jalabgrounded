package com.java7developer.chapter3.glab;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.java7developer.chapter3.listing_3_9.AgentFinderModule;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Code for listing 3_8
 */
//include as part of this lecture, how to use http://search.maven.org/
public class CurrenciesServiceClientHandRolled {

    private static Properties props = null;

    public static void main(String[] args) {

        //get properties
        props = new Properties();
        String strUserDir = System.getProperties().getProperty("user.dir");
        String strConifFilePath = strUserDir + "/db.properties";
        File file = new File(strConifFilePath);

       
        //use try with resources to load props
        try (FileInputStream input = new FileInputStream(file);) {

            props.load(input);

        } catch (Exception e) {

            System.out.println(e.getMessage());
        }
        
        //program to interfaces
        CurrencyFetcher currencyFetcher;
        
             if (Boolean.parseBoolean((String) props.get("dev"))) {
                    currencyFetcher = new LocalCurrencyFetcher();
                } else {
                     currencyFetcher = new RestfulCurrencyFetcher();
            }


        Map<String, String> map = currencyFetcher.fetchCurrencies();

        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " | " + entry.getValue());
        }

    }

}
