/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.java7developer.chapter3.glab;

/**
 *
 * @author ag
 */
public class CurrencyFetcherFactory {
    
    public enum ServiceType {
        REST, LOCAL
    }
    
    public static CurrencyFetcher getCurrencyFetcher(ServiceType type){
        
        
        switch(type){
            case REST:
                return new RestfulCurrencyFetcher();
               
            case LOCAL:
               return new LocalCurrencyFetcher();
             
            default:
                return new RestfulCurrencyFetcher();
        }
        

    }
    
    
}
