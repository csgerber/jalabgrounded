/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.java7developer.chapter3.glab;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author ag
 */
public class RestfulCurrencyFetcher implements CurrencyFetcher {

    @Override
    public Map<String,String> fetchCurrencies() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        Map<String,String> map = new TreeMap<>();
        
         org.json.JSONObject jsonRaw;
         JSONParser parser = new JSONParser();
         jsonRaw = parser.getJSONFromUrl("http://openexchangerates.org/api/latest.json?app_id=8a89405f4f574277897d20bdcac7706b");
        try {
            JSONObject jsonRates = jsonRaw.getJSONObject("rates");
            Iterator<?> keys = jsonRates.keys();

            while( keys.hasNext() ){
                String key = (String)keys.next();
               
                        map.put(key, jsonRates.get(key).toString());
              
            }

        } catch (JSONException ex) {
            Logger.getLogger(RestfulCurrencyFetcher.class.getName()).log(Level.SEVERE, null, ex);
        }
     
         
        return map;

    }
    
}
