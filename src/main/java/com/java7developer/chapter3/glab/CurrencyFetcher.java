/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.java7developer.chapter3.glab;

import com.google.inject.ImplementedBy;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//the default implementation for this. No need to bind
@ImplementedBy(RestfulCurrencyFetcher.class)
public interface CurrencyFetcher {
    //contract methods of this interface
     public Map<String,String> fetchCurrencies();
}
