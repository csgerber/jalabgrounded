/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.java7developer.chapter3.glab;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;


/**
 *
 * @author ag
 */
public class LocalCurrencyFetcher implements CurrencyFetcher {

    @Override
    public Map<String,String> fetchCurrencies() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Map<String,String> map = new TreeMap<>();
        map.put("USD", "1.000");
        map.put("GBP", "0.780");
        map.put("EUR", "0.965");
        
        return map;
     
    }
    
}
